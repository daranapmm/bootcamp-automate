package dexcom.automate.bootcamp.steps;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import pages.ComponentCardProducts;


public class StepsCardProducts {

    private ComponentCardProducts cardProducts;

    private WebDriver driver;

    public StepsCardProducts(final WebDriver driver) {

        this.driver = driver;
        cardProducts = new ComponentCardProducts(driver);
    }

    public boolean isCardProductsDisplayed(){

       return cardProducts.isDisplayed();
    }

    public void selectProduct(int index){
        cardProducts.getCardProducts().get(index).sendKeys(Keys.ENTER);
    }

}
