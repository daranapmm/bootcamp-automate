package dexcom.automate.bootcamp.steps;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pages.ComponentDescriptionProduct;
import utilities.ElementsActions;
import utilities.StringsTitles;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StepsDescriptionProduct  {

    private Map<String, String> listOfProducts;
    private ComponentDescriptionProduct componentDescriptionProduct;
    private ElementsActions elementsActions;
    private int numProducts;
    public StepsDescriptionProduct(final WebDriver driver){
        componentDescriptionProduct = new ComponentDescriptionProduct(driver);
        elementsActions             = new ElementsActions(driver);
        listOfProducts              = new HashMap<>();
    }

    public boolean isDisplayedImage(){
        return componentDescriptionProduct.isDisplayed();
    }
    private String productName(){
        return componentDescriptionProduct.getProductName().getText();
    }

    private String productPrice(){
        return componentDescriptionProduct.getProductPrice().getText();

    }

    private String productDescription(){
        return componentDescriptionProduct.getProductDescription().getText();
    }


    public void addToCar(int numProducts){

        for (int products=1; products<=numProducts; products++){
            componentDescriptionProduct.getProductAddToCar().click();
            elementsActions.switchToAlert();
            Assert.assertEquals(elementsActions.getTextAlert(), StringsTitles.ALERT_TEXT,"Mismatch the text alert");;
            elementsActions.acceptAlert();
            elementsActions.returnDefaultContent();
        }
    }


    public boolean isDisplayedAllDescriptionProduct(){

        if(isDisplayedImage()&&
                componentDescriptionProduct.getProductDescription().isDisplayed() &&
                     componentDescriptionProduct.getProductName().isDisplayed() &&
                         componentDescriptionProduct.getProductPrice().isDisplayed() &&
                                componentDescriptionProduct.getProductAddToCar().isDisplayed()
                                    ) {
            return true; }
        {
            return false;
        }
    }

    public Map<String, String> getAllDescriptionProduct(){
        return listOfProducts;
    }
}
