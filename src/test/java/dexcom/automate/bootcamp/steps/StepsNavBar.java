package dexcom.automate.bootcamp.steps;

import org.checkerframework.checker.units.qual.C;
import org.openqa.selenium.WebDriver;
import pages.ComponentNavBar;

public class StepsNavBar {


    private ComponentNavBar navBar;
    public StepsNavBar(final WebDriver driver) {

        this.navBar = new ComponentNavBar(driver);
    }

    public void goToCart(){
        navBar.getNavCart().click();
    }

    public void goToHome(){
        navBar.getNavHome().click();
    }

    public boolean isDisplayedNavBar(){

        return navBar.isDisplayed();
    }





}
