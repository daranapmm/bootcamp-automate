package dexcom.automate.bootcamp.steps;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.ComponentCategories;

import java.util.List;


public class StepsCategories  {


    private ComponentCategories componentCategories;

    public StepsCategories(final WebDriver driver){
        componentCategories = new ComponentCategories(driver);
    }


    public boolean isDisplayedCategories(){
        return  componentCategories.isDisplayed();
    }

    public List<WebElement> getListProducts(){

        return  componentCategories.getProducts();
    }

    public void goToPhones(){
        componentCategories.getPhones().click();
    }

    public void goLaptops(){
        componentCategories.getLaptops().click();
    }


}
