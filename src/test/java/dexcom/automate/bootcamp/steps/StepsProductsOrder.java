package dexcom.automate.bootcamp.steps;

import org.openqa.selenium.WebDriver;
import pages.ComponentProductsOrder;


import java.util.List;

public class StepsProductsOrder {

    private ComponentProductsOrder componentProductsOrder;

    public StepsProductsOrder(final WebDriver driver) {
        componentProductsOrder = new ComponentProductsOrder(driver);
    }


    public List<String> getPrices(){
        return componentProductsOrder.getTotalPrice();
    }

    public int getNumberOfProducts(){

       return componentProductsOrder.getNumOfRows();
    }

    public boolean isDisplayedTable(){
        return componentProductsOrder.isDisplayed();

    }

    public String getTotalOrder(){
        return componentProductsOrder.getTotalOder().getText();
    }

    public boolean theImagesAreDisplayed(){
        return componentProductsOrder.areDisplayedProducts();
    }

    public boolean theTitleProductsAreDisplayed(){
        return componentProductsOrder.areDisplayedProducts();
    }

    public boolean thePriceProductsAreDisplayed(){
        return componentProductsOrder.areDisplayedProducts();
    }


    public boolean theDeleteLinkProductsAreDisplayed(){
        return componentProductsOrder.areDisplayedProducts();
    }

    public String getButtonPlaceOrder(){
        return  componentProductsOrder.getBtnOrder().getText();
    }


}
