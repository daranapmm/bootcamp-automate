package dexcom.automate.bootcamp;

import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;



public class BaseTest {

    public static WebDriver driver = null;

    @BeforeClass
    public void setup() throws IOException {

        Properties properties = new Properties();


        FileInputStream fileInputStream = new FileInputStream(System.getProperty("user.dir")+"/src/main/java/properties/config.properties");
        properties.load(fileInputStream);

        String browserType  = properties.getProperty("browser").toLowerCase(Locale.ROOT).trim();

        switch (browserType) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/main/java/wedrivers/chromedriver");
                driver = new ChromeDriver();
                break;

            case "firefox":
                System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/src/main/java/drivers/geckodriver.exe");
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.setPageLoadStrategy(PageLoadStrategy.NONE);

                driver = new FirefoxDriver(firefoxOptions);

                break;

            case "edge":
                System.setProperty("webdriver.edge.driver", System.getProperty("user.dir") + "/src/main/java/drivers/msedgedriver.exe");
                EdgeOptions edgeOptions = new EdgeOptions();
                edgeOptions.setPageLoadStrategy("NORMAL");
                driver = new EdgeDriver(edgeOptions);
                break;
        }
        driver.manage().window().maximize();
    }

    @AfterClass
    public void tearDown () {
        driver.quit();
    }
}
