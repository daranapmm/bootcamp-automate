package dexcom.automate.bootcamp.tests;


import com.google.common.util.concurrent.Uninterruptibles;
import dexcom.automate.bootcamp.BaseTest;
import dexcom.automate.bootcamp.common.ListenerTestNG;
import dexcom.automate.bootcamp.steps.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utilities.AssertionsCustomized;
import utilities.StringsTitles;
import java.time.Duration;

@Listeners(ListenerTestNG.class)
public class
TestLaptops extends BaseTest {

    private StepsCategories stepsCategories;
    private StepsCardProducts cardProducts;
    private  AssertionsCustomized assertionsCustomized;
    private StepsDescriptionProduct stepsDescriptionProduct;
    private StepsProductsOrder stepsProductsOrder;
    private StepsNavBar navBar;



    @BeforeMethod
    public void setupPages(){

        stepsCategories = new StepsCategories(driver);
        cardProducts = new StepsCardProducts(driver);
        stepsDescriptionProduct = new StepsDescriptionProduct(driver);
        navBar                  = new StepsNavBar(driver);
        assertionsCustomized = new AssertionsCustomized();
        stepsProductsOrder = new StepsProductsOrder(driver);
    }

    @Test(description = "Validate products when is selected Laptops option negative case")
    public void validateLaptops(){
           driver.get("https://www.demoblaze.com/#");
           assertionsCustomized.isDisplayed(stepsCategories.isDisplayedCategories());
           assertionsCustomized.validateList(stepsCategories.getListProducts());
           stepsCategories.goLaptops();
           assertionsCustomized.isDisplayed(cardProducts.isCardProductsDisplayed());
           cardProducts.selectProduct(1);
           assertionsCustomized.assertTrue(stepsDescriptionProduct.isDisplayedAllDescriptionProduct());
           stepsDescriptionProduct.addToCar(2);
           assertionsCustomized.isDisplayed(navBar.isDisplayedNavBar());
           navBar.goToCart();
           assertionsCustomized.isDisplayed(stepsProductsOrder.isDisplayedTable());
           assertionsCustomized.validateProducts(stepsProductsOrder.getNumberOfProducts(),3);
           assertionsCustomized.validateTotalPrice( stepsProductsOrder.getPrices(),stepsProductsOrder.getTotalOrder());
           assertionsCustomized.isDisplayed(stepsProductsOrder.theImagesAreDisplayed());
           assertionsCustomized.isDisplayed(stepsProductsOrder.theTitleProductsAreDisplayed());
           assertionsCustomized.isDisplayed(stepsProductsOrder.thePriceProductsAreDisplayed());
           assertionsCustomized.isDisplayed(stepsProductsOrder.theDeleteLinkProductsAreDisplayed());
           assertionsCustomized.validateText(stepsProductsOrder.getButtonPlaceOrder(), StringsTitles.BUTTON_PLACE_ORDER);

        Uninterruptibles.sleepUninterruptibly(Duration.ofSeconds(3));
    }


}
