package dexcom.automate.bootcamp.common;


import org.testng.ITestListener;
import org.testng.ITestResult;


public class ListenerTestNG extends TakeScreenshot implements ITestListener{


    @Override
    public void onTestFailure(ITestResult result) {
        getScreenshot(result);
    }
}
