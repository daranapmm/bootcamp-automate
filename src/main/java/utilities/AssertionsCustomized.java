package utilities;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.asserts.Assertion;
import java.util.List;
import java.util.stream.Collectors;


public class AssertionsCustomized extends Assertion {


    public  void  validateText(String actualText, String expectedText){
        assertEquals(actualText,expectedText);
    }

    public void isDisplayed(boolean isDisplayed){
        assertTrue(isDisplayed,"Element is displayed");

    }
    
    public void  validateList(List<WebElement> elementsOfList){
        
        elementsOfList.stream().forEach(element -> {
            Assert.assertTrue(element.isDisplayed());
            System.out.println("The element is " +element.getText());
        });
    }

    public void validateTotalPrice(List<String> actualPrice, String expectedPrice){

        final int expectedPriceInt = Integer.parseInt(expectedPrice);

        final List<Integer> priceToInt = actualPrice.
                stream().map(Integer::parseInt).collect(Collectors.toList());

        Integer priceConvertToInt = priceToInt.
                stream()
                .mapToInt(Integer::intValue)
                .sum();
        Assert.assertEquals(java.util.Optional.of(priceConvertToInt), java.util.Optional.of(expectedPriceInt));
    }

    public void validateProducts(int numProducts, int productsExpected){
        Assert.assertEquals(numProducts, productsExpected,"Were added only " +numProducts+" products");
    }

}
