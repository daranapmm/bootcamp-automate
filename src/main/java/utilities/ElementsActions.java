package utilities;


import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.AbstractPage;



public class ElementsActions  extends AbstractPage {

   private WebDriver driver;
    private Alert alert;


    @Override
    public boolean isDisplayed() {
        return false;
    }

    public ElementsActions(final WebDriver driver){
        super(driver);
        this.driver = driver;
    }

    public void switchToAlert(){
        webDriverWait.until(ExpectedConditions.alertIsPresent());
        alert = driver.switchTo().alert();
    }

    public void acceptAlert(){
        alert.accept();
    }
    public String getTextAlert(){
        return alert.getText();
    }

    public void returnDefaultContent(){
        driver.switchTo().defaultContent();
    }


}

