package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


import java.util.List;

public class ComponentCardProducts extends AbstractPage{


    @FindBy(xpath = "//div[@class='card h-100']/a")
    List<WebElement> cardProducts;


    public ComponentCardProducts(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getCardProducts(){

        return cardProducts;
    }

    @Override
    public boolean isDisplayed() {
        return webDriverWait.until(cardProduct -> this.cardProducts.size() > 1);
    }
}
