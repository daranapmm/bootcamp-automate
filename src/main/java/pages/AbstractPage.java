package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;



import java.time.Duration;

public abstract class AbstractPage  {

    public abstract boolean isDisplayed();

    protected  Wait<WebDriver> webDriverWait;


    public AbstractPage(final WebDriver driver){
        webDriverWait  = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(10))
                .pollingEvery(Duration.ofMillis(500))
                .ignoring(NoSuchElementException.class);
        PageFactory.initElements(driver,this);

    }

}
