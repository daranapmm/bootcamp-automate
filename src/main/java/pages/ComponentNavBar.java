package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ComponentNavBar extends AbstractPage{




    @FindBy(how = How.CSS, using = ".container")
    private WebElement navContainer;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Home')]")
    private WebElement navHome;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'About us')]")
    private WebElement navContact;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Cart')]")
    private WebElement navCart;

    public ComponentNavBar(final WebDriver driver) {
        super(driver);
    }


    public WebElement getNavHome() {
        return navHome;
    }

    public WebElement getNavContact() {
        return navContact;
    }

    public WebElement getNavCart() {
        return navCart;
    }

    public WebElement getNavContainer() {
        return navContainer;
    }

    @Override
    public boolean isDisplayed() {
        return webDriverWait.until(navCont -> this.navContainer.isDisplayed());
    }
}
