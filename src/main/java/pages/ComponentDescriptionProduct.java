package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ComponentDescriptionProduct extends AbstractPage{



    @FindBy(how = How.CSS, using = ".name")
    private WebElement productName;

    @FindBy(how = How.CSS, using = ".price-container")
    private WebElement ProductPrice;

    @FindBy(how = How.CSS, using = "#more-information")
    private WebElement productDescription;

    @FindBy(how = How.CSS, using = "div#imgp  img")
    private WebElement productImage;

    @FindBy(how = How.XPATH, using = "//a[@class='btn btn-success btn-lg']")
    private WebElement productAddToCar;



    public ComponentDescriptionProduct(WebDriver driver) {
        super(driver);
    }

    public WebElement getProductName() {
        return productName;
    }

    public WebElement getProductPrice() {
        return ProductPrice;
    }

    public WebElement getProductDescription() {
        return productDescription;
    }


    public WebElement getProductAddToCar() {
        return productAddToCar;
    }

    @Override
    public boolean isDisplayed() {
        return webDriverWait.until(productImage -> this.productImage.isDisplayed());
    }

}
