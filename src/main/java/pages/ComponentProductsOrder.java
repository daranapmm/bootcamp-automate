package pages;

import com.google.common.util.concurrent.Uninterruptibles;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utilities.StringsTitles;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class ComponentProductsOrder extends AbstractPage{

    private WebDriver driver;
    private String listTableProducts = "//tbody[@id='tbodyid']/tr";

    @FindBy(how = How.XPATH, using = "//button[@class='btn btn-success']")
    WebElement btnOrder;

    @FindBy(how = How.ID, using = "totalp")
    WebElement totalOder;

    @FindBy(how = How.XPATH, using = "//tbody[@id='tbodyid']/tr")
    List<WebElement> numOfRows;

    @FindBy(how = How.XPATH, using = "//tbody[@id='tbodyid']/tr[1]/td")
    List<WebElement> numOColumns;

    public ComponentProductsOrder(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }


    public WebElement getTotalOder() {
        return totalOder;
    }


    public int getNumOfRows() {
        return numOfRows.size();
    }

    private int  getNumOColumns() {
        return numOColumns.size();
    }



    public List<String> getTotalPrice(){
        List<String> finalPrice = new ArrayList<>();
        for(int i =1;i<=getNumOfRows(); i++){
            for (int k=1;k<=getNumOColumns(); k++){
                if(k==3){
                    finalPrice.add(driver.findElement(By.xpath("//tbody[@id='tbodyid']/tr["+i+"]/td["+k+"]")).getText());
                }
            }
        }
        return finalPrice;
    }

    public boolean areDisplayedProducts(){
       boolean productDisplayed =false;
        for(int i =1;i<=getNumOfRows(); i++){
            for (int k=1;k<=getNumOColumns(); k++){

                if(k==1 && StringsTitles.IMAGES_DISPLAYED==1){
                    driver.findElement(By.xpath("//tbody[@id='tbodyid']/tr["+i+"]/td["+k+"]")).isDisplayed();
                    productDisplayed = true;
                }else if (k==2 && StringsTitles.TITLES_DISPLAYED==2){
                    driver.findElement(By.xpath("//tbody[@id='tbodyid']/tr["+i+"]/td["+k+"]")).isDisplayed();
                    productDisplayed = true;
                }else if (k==3 && StringsTitles.PRICE_DISPLAYED==3){
                    driver.findElement(By.xpath("//tbody[@id='tbodyid']/tr["+i+"]/td["+k+"]")).isDisplayed();
                    productDisplayed = true;
                }else if(k==4 && StringsTitles.DELETE_DISPLAYED==4 ){
                    driver.findElement(By.xpath("//tbody[@id='tbodyid']/tr["+i+"]/td["+k+"]")).isDisplayed();
                    productDisplayed = true;
                }
            }
        }
        return productDisplayed;
    }

    public WebElement getBtnOrder() {
        return btnOrder;
    }


    @Override
    public boolean isDisplayed() {
        webDriverWait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(listTableProducts)));
        return webDriverWait.until(table -> this.numOfRows.size() >= 1);
    }
}
