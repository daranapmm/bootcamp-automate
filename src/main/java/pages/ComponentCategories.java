package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import java.util.List;


public class ComponentCategories extends AbstractPage{


    @FindBy(linkText = "CATEGORIES")
    WebElement categories;

    @FindBy(linkText = "Monitors")
    WebElement monitors;

    @FindBy(how = How.LINK_TEXT, using = "Phones")
    WebElement phones;


    @FindBy(how = How.LINK_TEXT, using = "Laptops")
    WebElement laptops;

    @FindBy(css ="a#itemc")
    List<WebElement> products;



    public ComponentCategories(final WebDriver driver){
        super(driver);
    }


    public List<WebElement> getProducts (){
        return  products;
    }

    public WebElement getMonitors(){
        return monitors;
    }

    public WebElement getPhones(){
        return phones;
    }

    public WebElement getLaptops(){
        return laptops;
    }
    @Override
    public boolean isDisplayed() {
        return this.webDriverWait.until(categories -> this.categories.isDisplayed());
    }
}
