##[Project Bootcamp Automate with selenium] 


> [!IMPORTANT]
###Install Maven in the system
###Before execute install maven libraries and dependencies.
######The chrome driver it is optimized for Chrome 108, if Chrome version is different check it in About Chrome and download the correct version from this link.
- https://chromedriver.chromium.org/downloads

- [ ] **How run it**
  
    -From IDE  Only run the testng.xml file, just right click
     and select "Run".
  
    -From command line: In the terminal go to root project bootcamp
     execute the next command: **mvn clean  test -D testng.xml** 



- [ ] **How see the results**
     ###Execution testng.xml file directly with IDE
    -Once completed the test go to folder **"test-output"** and open the file index.html.
        
    -The project has ability to attach the screenshots into report when a test fails,
          for that in the report go to **"Reporter output"**.
  
    -There is a carpet with screenshots where are added the captures when a test fails.

   ###Execution with Maven 
    -Once completed the test go to folder **"target > surefire-reports > index.html"** and open the file index.html.
        
    -The project has ability to attach the screenshots into report when a test fails,
          for that in the report go to **"Reporter output"**.
  
    -There is a carpet with screenshots where are added the captures when a test fails.


###Manual test cases were included as PDF file and requirements at root of this project.
   -Manual Tests Cases Bootcamp ITJ.pdf

   -Capstone_Project_ITJ_Automation_Bootcamp_2

### The requirements are covered in two testcases
    -1 TesLaptops as negative test case.
    -2 TestPhones as positive test case.





        

    


